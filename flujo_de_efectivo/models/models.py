# -*- coding: utf-8 -*-

from odoo import api, fields, models

# class flujo_de_efectivo(models.Model):
#     _name = 'flujo_de_efectivo.flujo_de_efectivo'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100




class CashFlow(models.Model):
    _name = 'cash.flow'
    name = fields.Char(String="Title", required=True)
    start_date = fields.Date(default=fields.Date.today, required=True)
    final_date = fields.Date(default=fields.Date.today,required=True)
    date =fields.Date(default=fields.Date.today)
    to_show = fields.Selection([
    	('week','Week'),
    	('Month','month'),
    	('year','Year'),
    	],default='week')
    start_of_cash = fields.Float(String = "Start of Cash",required=True)


    

    def manejo_Cadena(self,Cadena):
        text = str(Cadena)
        x = text.replace("'(u'", "")
        x2= x.replace ("',)'","")
        #return x2

    def Obtener_AsientosContablesEgresos(self):
        Query= "select account_account.name from account_account  join account_account_type  on account_account_type.name ='Expenses' and account_account.user_type_id = account_account_type.id"
        self.env.cr.execute(Query);
        Nombres_Egresos = self.env.cr.fetchall()
        print(Nombres_Egresos)
        return Nombres_Egresos

    def Obtener_AsientosContablesIngresos(self):
        List = []
        Query= "select account_account.name from account_account  join account_account_type  on account_account_type.name ='Income' and account_account.user_type_id = account_account_type.id"
        print(" QUERY " +Query);
        self.env.cr.execute(Query);
        Nombres_Ingresos = self.env.cr.fetchall()
        for i in Nombres_Ingresos:
            List.append(self.manejo_Cadena(i))
            print self.manejo_Cadena(i)
        print "JJJJJJJJJJJJJJJJJJJJJJJJJJJ"
        print List
        return List

    def Obtener_Ingresos_Periodo01(self):
        List = []
        for i in self.Obtener_AsientosContablesIngresos():

            Query = "select sum(account_invoice_line.price_subtotal_signed) from account_invoice_line  join account_invoice on account_invoice_line.invoice_id = account_invoice.id  and account_invoice.state = 'paid'  join account_account on account_invoice_line.account_id = account_account.id  and account_account.name='"+str(i)+"'"
            print "++++++++++++++++++++++++++++++++++++"
            print Query
            print "+++++++++++++++++++++++++++++++++++"
            self.env.cr.execute (Query)
            List.append(self.env.cr.fetchone())
        print(List)


    @staticmethod
    def get_NameMonths(Current_month):
       
        List  =  []
        Meses = ['JANUARY','FEBRUARY','MARCH','APRIL','MAY','JUNE','JULY','AUGUST','SEPTEMBER','OCTOBER','NOVEMBER','DECEMBER']
        List.append(Meses[Current_month])

        for i in range(3):
            
            Current_month= Current_month + 1
            
            
            if (Current_month==12):
                Current_month = 1
            
            List.append(Meses[Current_month])
            
            
        
        return List

    def get_Namecolumns(self):
        list = []
        data = ""
        print("Def : get_Namecolumns ")
        if self.to_show == 'week':
         
         list = ["Semana 1, Semana 2 , Semana 3 , Semana 4"]
         

        if self.to_show == 'Month':
         valor = self.start_date
         data = valor.split("-")
         if(int(data[1]) == 10):
            data = int(data[1])-1
            list = self.get_NameMonths(data)
         if(int(data[1]!=10)):
            data = data[1].replace("0","")
            data = int(data)-1
            #self.get_NameMonths(data)
            list = self.get_NameMonths(data)
        
        
        print(list)
        return list

    
    

class Table_CashFlow(models.Model):
    _name = 'table.cashflow'




        


  



